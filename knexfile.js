var dotenv = require('dotenv');
dotenv.load();

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      host     : process.env.DABSTER_MYSQL_HOST,
      port     : process.env.DABSTER_MYSQL_PORT,
      user     : process.env.DABSTER_MYSQL_USER,
      password : process.env.DABSTER_MYSQL_PASS,
      database : 'dabster_email_dev'
    },
    debug: false,
    pool: {
	    max: 1,
	    min: 1
	  },
    seeds: {
        directory: './seeds'
    }
  },
  test: {
    client: 'mysql',
    connection: {
      host     : process.env.DABSTER_MYSQL_HOST,
      port     : process.env.DABSTER_MYSQL_PORT,
      user     : process.env.DABSTER_MYSQL_USER,
      password : process.env.DABSTER_MYSQL_PASS,
      database : 'dabster_email_test'
    },
    pool: {
      max: 1,
      min: 1
    },
  
  },
  production: {
    client: 'mysql',
    connection: {
      host     : process.env.DABSTER_MYSQL_HOST,
      port     : process.env.DABSTER_MYSQL_PORT,
      user     : process.env.DABSTER_MYSQL_USER,
      password : process.env.DABSTER_MYSQL_PASS,
      database : 'dabster_email'
    },
    pool: {
      max: 1,
      min: 1
    },
    
  }
}