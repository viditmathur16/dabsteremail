if (process.env.ENV != "production") {
  require('dotenv').config();
}

const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');

// Routes imports
const index = require('./routes/index');
const templateCategory = require('./routes/templateCategory');
const template = require('./routes/template');
const address = require('./routes/address');
const groups = require('./routes/groups');
const user = require('./routes/user');
const campaign = require('./routes/campaign');
const sendEmail = require('./routes/sendEmail');
const emailhistory = require('./routes/emailhistory');

let app = express();

app.set('trust proxy', true);
app.use(helmet());


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type,x-access-token, x-access-user, Accept");
  next();
});

// Healthcheck
app.use('/', index);
// // Routes
app.use('/mail', sendEmail);
app.use('/mail', emailhistory);
app.use('/templateCategory', templateCategory);
app.use('/template', template);
app.use('/campaign', campaign);
app.use('/address', address);
app.use('/group', groups);
app.use('/user', user);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({
    success: false,
    message: res.locals.message,
    error: res.locals.error
  });
});
require('./server');

module.exports = app;
