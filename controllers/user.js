const mute = require('immutable');
const moment = require('moment');
const Randomstring = require('randomstring');
const async = require('async');
const users = require('../models/user')
const utilities = require('../helpers/security')
const emailProviders = require('../seeds/emailProviders')

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "organisation",
    action: null,
    id: null,
    data: null,
    status: null
});

// // insert organiation
// const insertUser = function (data, cb) {
//     if (!data.name || !data.email || !data.domain) {
//         return cb(responseStruct.merge({
//             success: false,
//             message: `params missing`,
//             status: 400,
//         }).toJS());
//     }
//     let insertData = {
//         name: data.name,
//         email: data.email,
//         domain: data.domain,
//         accountId: data.accountId,
//     }
//     organisation.insert(insertData, function (err, resp) {
//         if (err) {
//             console.log(err);
//             throw (err);
//         }
//         return cb(null, responseStruct.merge({
//             success: true,
//             message: `new organisation added`,
//             status: 200
//         }).toJS());
//     })
// };

// exports.insertUser = insertUser

//signup
const userSignup = function (data, response, cb, isSocial) {
    if (!cb) {
        cb = response;
    }
    async.waterfall([
        async.apply(checkDuplicate, data),
        async.apply(generatePassword, data),
        async.apply(generateAccountId, data),
        async.apply(insertUser, data)
    ], cb);
};
exports.userSignup = userSignup;


const checkDuplicate = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    if (emailProviders.emailProviders.includes(data['email'].split('@')[1])) {
        return cb(responseStruct.merge({
            signature: data.req.signature,
            action: "checkExisitingEmail",
            status: 400,
            success: false,
            message: "please signup using your professional email",
        }).toJS());
    }
    users.getByEmail({ email: data.email }, function (err, result) {
        if (err) {

            console.log("Unable to fetch user by email" + err);

            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "checkExisitingEmail",
                status: 500,
                success: false,
                message: "something went wrong",
            }).toJS());
        }
        if (result.length > 0) {
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "checkExisitingEmail",
                status: 400,
                success: false,
                message: "user exists with this email",
            }).toJS());

        }
        return cb(null,
            responseStruct.merge({
                signature: data.req.signature,
                action: "checkExisitingEmail",
                status: 200,
                success: true,
                message: ""
            }).toJS());
    });
};
const generatePassword = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    utilities.generatePassword(data.password, function (err, hash_data) {
        if (err) {

            console.log("Unable to fetch user by email" + err);

            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "securing_password",
                status: 500,
                success: false,
                message: "something went wrong",
            }).toJS());
        }
        return cb(null,
            responseStruct.merge({
                signature: data.req.signature,
                action: "securing_password",
                status: 200,
                success: true,
                message: "",
                data: hash_data
            }).toJS());
    });
};

const generateAccountId = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }

    let accountId = Randomstring.generate(6)
    let forwardData = data;
    forwardData.accountId = accountId;
    forwardData.hash_data = response.data;

    return cb(null,
        responseStruct.merge({
            signature: data.req.signature,
            action: "generate account id",
            status: 200,
            success: true,
            message: "",
            data: forwardData
        }).toJS());

};

const insertUser = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    responseStruct.action = "insertUser";

    let account_id = null;
    let salt = null;
    if (response.data) {
        salt = response.data.hash_data.salt;
        account_id = response.data.accountId;
    }

    if (!account_id) {
        return cb(responseStruct.merge({
            signature: data.req.signature,
            action: "insert_user",
            status: 400,
            success: false,
            message: "Missing AccountID",
        }).toJS());
    }

    let insert_data = {}
    insert_data.firstName = data.firstName;
    insert_data.lastName = data.lastName;
    insert_data.accountId = account_id;
    insert_data.email = data.email;
    insert_data.mobile = data.mobile;
    insert_data.gender = data.gender;
    insert_data.birthday = data.birthday;
    insert_data.organisation = data.organisation;
    insert_data.designation = data.designation;
    insert_data.salt = salt;

    users.insert(insert_data, function (err, ignore) {
        if (err) {
            console.log("Unable to insert user" + err);

            return cb(
                responseStruct.merge({
                    signature: data.req.signature,
                    status: 520,
                    success: false,
                    message: 'something went wrong',
                    action: "insertUser"
                }).toJS());
        }
        return cb(null,
            responseStruct.merge({
                signature: data.req.signature,
                status: 200,
                success: true,
                message: 'Inserted User',
                action: "insertUser"
            }).toJS());
    })

};

//login
const userLogin = function (data, cb, isSocial) {
    if (!data.email && (!data.mobile || !data.countryCode)) {
        return cb(responseStruct.merge({
            signature: data.req.signature,
            action: "userLogin",
            status: 500,
            success: false,
            message: "Email is required",
        }).toJS());
    }

    let functionsWaterfall = [];
    functionsWaterfall.push(async.apply(readByKeyValue, data));
    functionsWaterfall.push(async.apply(updateLoginInfo, data));
    functionsWaterfall.push(async.apply(encryptData, data));

    async.waterfall(functionsWaterfall, cb);
};
exports.userLogin = userLogin;

const readByKeyValue = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    let login_req = {}
    //second priority
    if (data.email) {
        login_req = {
            key: 'email',
            value: data.email
        }
    }
    if (data.identifier) {
        login_req = {
            key: 'accountId',
            value: data.identifier
        }
    }
    users.readByKeyValue(login_req, function (err, user) {
        if (err) {
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "user_login",
                status: 500,
                success: false,
                message: "Something went wrong!",
            }).toJS());
        }

        if (user.length <= 0) {
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "user_login",
                status: 401,
                success: false,
                message: "Invalid ID and Password Combination.",
            }).toJS());
        } else {
            return cb(null,
                responseStruct.merge({
                    signature: data.req.signature,
                    action: "user_login",
                    status: 200,
                    success: true,
                    message: "ok",
                    data: user[0]
                }).toJS());
        }
    });
}

const updateLoginInfo = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    let login_req = {
        id: response.data.id
    }

    users.updateLoginInfo(login_req, function (err, user) {
        if (err) {
            console.log(`Unable to update user login info.` + err);
            throw err;
        }
    });

    return cb(null,
        responseStruct.merge({
            signature: data.req.signature,
            action: "user_login",
            status: 200,
            success: true,
            message: "Username - Password Matches",
            data: response.data
        }).toJS());
}

const encryptData = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    let user_data = {
        id: response.data.id,
        email: response.data.email,
        mobile: response.data.mobile || "none",
        accountId: response.data.accountId,
        firstName: response.data.firstName,
        lastName: response.data.lastName,
        gender: response.data.gender,
        profilePicture: response.data.profilePicture,
        created_at: Date.now(),
        token_type: "auth"
    };

    const encKey = response.data.salt;

    utilities.encryptData(user_data, encKey, function (err, cipher) {
        if (err) {

            console.log(`Unable to encrypt data.` + err);
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "user_login",
                status: 500,
                success: false,
                message: "Something went wrong!",
            }).toJS());
        }
        return cb(null,
            responseStruct.merge({
                signature: data.req.signature,
                action: "user_login",
                status: 200,
                success: true,
                message: "Successfully Logged In!",
                data: {
                    id: response.data.id,
                    email: response.data.email,
                    accountId: response.data.accountId,
                    firstName: response.data.firstName,
                    lastName: response.data.lastName,
                    gender: response.data.gender,
                    profilePicture: response.data.profilePicture,
                    sessionToken: cipher
                }
            }).toJS());
    });
}

const updateProfile = function (data, response, cb) {
    if (!cb) {
        cb = response;
    }
    if (!data.id) {
        return cb(responseStruct.merge({
            signature: data.req.signature,
            action: "updateUserProfile",
            status: 400,
            success: true,
            message: "plarams missing"
        }).toJS());
    }
    let user_data = {
        id: data.id,
        user: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        gender: data.gender,
        isDeleted: data.isDeleted,
        isDisabled: data.isDisabled,
        isVerified: data.isVerified,
        organisation: data.organisation,
        designation: data.designation,
        email: data.email,
        mobile: data.mobile,
        username: data.username,
        birthday: data.birthday,
        apiKeyAdded: data.apiKeyAdded,
        apiVersion: data.apiVersion,
        accessKey: data.accessKey,
        secretAccessKey: data.secretAccessKey,
        region: data.region,
        updated_at: Date.now()
    };
    users.updateProfile(user_data, function (err, response) {
        if (err) {
            console.log(err);
            throw err;
        }
        return cb(null, responseStruct.merge({
            signature: data.req.signature,
            action: "update profile",
            status: 200,
            success: true,
            message: "updated user profile"
        }).toJS());
    })

}
exports.updateProfile = updateProfile;


const validate_token = function (data, cb) {
    // console.log("data", data);
    if (!data.token || !data.identifier) {
        return cb(responseStruct.merge({
            signature: data.req.signature,
            action: "validate_token",
            status: 403,
            success: false,
            message: "Invalid Credentials",
        }).toJS());
    }
    tokenPayload = {
        user: data.identifier,
        token: data.token
    };

    readByKeyValue(data, function (err, user) {
        if (err) {
            console.log(err)
            return cb(err);
        }
        //console.log("user",user)
        // console.log("user",data)
        if (!user) {
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "validate_token",
                status: 403,
                success: false,
                message: "",
            }).toJS());
        }
        if (!user.data) {
            return cb(responseStruct.merge({
                signature: data.req.signature,
                action: "validate_token",
                status: 403,
                success: false,
                message: "",
            }).toJS());
        } else {
            if (!user.data) {
                return cb(responseStruct.merge({
                    signature: data.req.signature,
                    action: "validate_token",
                    status: 403,
                    success: false,
                    message: "",
                }).toJS());
            }
        }
        let key = user.data.salt;
        utilities.decryptData(data.token, key, function (err, response) {
            if (err) {
                console.log(err);
                return cb(responseStruct.merge({
                    signature: data.req.signature,
                    action: "validate_token",
                    status: 403,
                    success: false,
                    message: "",
                }).toJS());
            }
            return cb(null,
                responseStruct.merge({
                    signature: data.req.signature,
                    action: "validate_token",
                    status: 200,
                    success: false,
                    message: "",
                    data: response
                }).toJS());
        })
    })
};

exports.validate_token = validate_token;
