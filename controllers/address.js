const mute = require('immutable');
const uuid = require('uuid').v4;
const addressBookSchema = require('../models/mongoDb/addressBook')
// const db = require('../models/mongoDb/db').model;
let mongoose = require('../models/mongoDb/db');

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "address",
    action: null,
    id: null,
    data: null,
    status: null
});

// insert addressBook
const insertAddressBook = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.addressBook) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!data.groups || !data.groups.length) {
        data.groups = []
    }

    if (!Array.isArray(data.addressBook)) {
        data.addressBook = [data.addressBook]
    }
    data.addressBook.forEach(add => {
        // let name_arr = add.name.toString().spit(" ");
        // if (name_arr.length === 3) {
        //     add.firstName = add.firstName || name_arr[0]
        //     add.middleName = add.middleName || name_arr[1]
        //     add.lastName = add.lastName || name_arr[2]
        // } else if (name_arr.length === 2) {
        //     add.firstName = add.firstName || name_arr[0]
        //     add.middleName = add.middleName || ""
        //     add.lastName = add.lastName || name_arr[1]
        // } else {
        //     add.firstName = add.firstName || name_arr[0]
        //     add.middleName = add.middleName || ""
        //     add.lastName = add.lastName || ""
        // }
        add.id = uuid()
        add.birthday = add.birthday
        add.firstName = add.firstName
        add.lastName = add.lastName
        add.middleName = add.middleName
        add.isDeleted = false
        add.status = add.status
        add.name = add.name
        add.email = add.email
        add.user = add.user.toString()
        add.groups = add.groups
    });
    mongoose.connection.db.collection('addressbook_developments', action);
    function action(err, collection) {
        collection.insert(data.addressBook, function (err, resp) {
            if (err) {
                console.log(err);
                throw err;
            } else {
                return cb(null, responseStruct.merge({
                    success: true,
                    data: resp,
                    message: `new addressBook added`,
                    status: 200
                }).toJS());
            }
        });
    }
};

exports.insertAddressBook = insertAddressBook


// fetch all address
const fetchAddressBook = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    addressBookSchema.find({ user: data.user }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: ` addressBook found`,
            status: 200,
            data: resp
        }).toJS());
    })
};
exports.fetchAddressBook = fetchAddressBook

// fetch  address by id
const fetchAddressBookById = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    addressBookSchema.find({ user: data.user, id: data.id }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `addressBook found`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.fetchAddressBookById = fetchAddressBookById

// fetch  address by group
const fetchAddressBookByGroups = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    data.group = data.group.split(',')

    addressBookSchema.find({ groups: { $in: data.group }, user: data.user }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `addressBook group found`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.fetchAddressBookByGroups = fetchAddressBookByGroups

// update  address by group
const updateAddressBook = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!Array.isArray(data.group)) {
        data.group = [data.group]
    }
    let update_data = {}
    if (data.name) {
        update_data['name'] = data.name
    }
    if (data.firstName) {
        update_data['firstName'] = data.firstName
    }
    if (data.middleName) {
        update_data['middleName'] = data.middleName
    }
    if (data.lastName) {
        update_data['lastName'] = data.lastName
    }
    if (data.email) {
        update_data['email'] = data.email
    }

    if (data.user) {
        update_data['user'] = data.user
    }
    if (data.phone) {
        update_data['phone'] = data.phone
    }
    if (data.comment) {
        update_data['comment'] = data.comment
    }
    if (data.birthday) {
        update_data['birthday'] = data.birthday
    }
    if (data.groups) {
        update_data['groups'] = data.groups
    }
    if (data.isDeleted === "true") {
        update_data['isDeleted'] = true
    }
    if (data.status) {
        update_data['status'] = data.status
    }

    // get the current date
    let currentDate = new Date();
    // change the updated_at field to current date
    update_data.updated_at = currentDate;
    addressBookSchema.updateOne({ id: data.id }, update_data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `address book updated`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.updateAddressBook = updateAddressBook

// add groups to address 
const addGroupToMultipleAddress = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user || !data.group) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!Array.isArray(data.address)) {
        data.address = [data.address]
    }
    let length = data.address.length;
    function updateAll() {
        let doc = data.address.pop();
        // get the current date
        let currentDate = new Date();
        // change the updated_at field to current date
        doc.updated_at = currentDate;

        addressBookSchema.updateOne({ id: doc, user: data.user }, { updated_at: currentDate, $push: { groups: data.group } }, function (err, resp) {
            if (err) {
                console.log(err);
                throw err;
            }
            if (--length) {
                updateAll();
            } else {
                return cb(null, responseStruct.merge({
                    success: true,
                    message: `new group to addresses added`,
                    status: 200
                }).toJS());
            }
        })
    }
    updateAll()
};

exports.addGroupToMultipleAddress = addGroupToMultipleAddress

// delte groups to multiple address 
const deleteGroupToMultipleAddress = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user || !data.group) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!Array.isArray(data.address)) {
        data.address = [data.address]
    }
    let length = data.address.length;
    function updateAll() {
        let doc = data.address.pop();
        // get the current date
        let currentDate = new Date();
        // change the updated_at field to current date
        doc.updated_at = currentDate;

        addressBookSchema.updateOne({ id: doc, user: data.user }, { updated_at: currentDate, $pull: { groups: data.group } }, function (err, resp) {
            if (err) {
                console.log(err);
                throw err;
            }
            if (--length) {
                updateAll();
            } else {
                return cb(null, responseStruct.merge({
                    success: true,
                    message: `new group to addresses added`,
                    status: 200
                }).toJS());
            }
        })
    }
    updateAll()
};

exports.deleteGroupToMultipleAddress = deleteGroupToMultipleAddress

// add groups to single address 
const addAddressBookGroups = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user || !data.group) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!Array.isArray(data.group)) {
        data.group = [data.group]
    }
    let currentDate = new Date();
    addressBookSchema.updateOne({ id: data.id }, { updated_at: currentDate, $push: { groups: { $each: data.group } } }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `new group added`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.addAddressBookGroups = addAddressBookGroups

// get distinct groups to address 
const getDistinctAddressGroups = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    addressBookSchema.distinct('groups', { 'user': data.user }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `user groups`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.getDistinctAddressGroups = getDistinctAddressGroups

// delete address 
const deleteAddress = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    addressBookSchema.deleteOne({ user: data.user, id: data.id }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `user groups`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.deleteAddress = deleteAddress
