const mute = require('immutable');
const uuid = require('uuid').v4;
const groupSchema = require('../models/mongoDb/groups')

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "group",
    action: null,
    id: null,
    data: null,
    metadata: null,
    status: null
});

// insert addressBook
const insertGroup = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.name || !data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let groupCode = String(`${data.user}-${data.name.split(' ').join('-').toString()}`).toLowerCase()
    let group = new groupSchema({
        id: uuid(),
        name: data.name,
        user: data.user,
        status: data.status,
        isDeleted: false,
        description: data.description,
        groupCode: groupCode
    })
    group.save(function (err, saved) {
        if (err) {
            console.log(err);
            throw err;
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `new group added`,
            status: 200
        }).toJS());

    })
};

exports.insertGroup = insertGroup


// fetch all Groups
const fetchGroups = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    groupSchema.find({ user: data.user }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: ` group found`,
            status: 200,
            data: resp
        }).toJS());
    })
};
exports.fetchGroups = fetchGroups

// fetch  address by id
const fetchGroupById = function (data, cb) {
    // data.user = data.req.auth.id

    if (!data.user || !data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    groupSchema.find({ user: data.user, id: data.id }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        if (resp.length > 0) {
            groupSchema.find({ user: data.user, id: data.id, status: 'active' }, function (err, response) {
                if (err) {
                    console.log(err);
                    throw (err);
                }
                return cb(null, responseStruct.merge({
                    success: true,
                    message: ` group found`,
                    status: 200,
                    data: resp[0],
                    metadata: {
                        activeCount: response.length
                    }
                }).toJS());
            })
        }else{
            return cb(null, responseStruct.merge({
                success: true,
                message: `no group found`,
                status: 200,
                data: [],
                metadata: {
                    activeCount: 0
                }
            }).toJS());
        }
    })
};

exports.fetchGroupById = fetchGroupById

// update  group
const updateGroup = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!Array.isArray(data.group)) {
        data.group = [data.group]
    }
    let update_data = {}
    if (data.name) {
        update_data['name'] = data.name
    }
    if (data.status) {
        update_data['status'] = data.status
    }
    if (data.description) {
        update_data['description'] = data.description
    }
    if (data.isDeleted === "true") {
        update_data['isDeleted'] = true
    }

    groupSchema.updateOne({ id: data.id }, update_data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `group updated`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.updateGroup = updateGroup

// delete group
const deleteGroup = function (data, cb) {
    // data.user = data.req.auth.id
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    groupSchema.deleteOne({ user: data.user, id: data.id }, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `group deleted`,
            status: 200,
            data: resp
        }).toJS());
    })
};

exports.deleteGroup = deleteGroup
