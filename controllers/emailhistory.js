const mute = require('immutable');

const emailhistory = require('../models/emailhistory')

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "emailhistory",
    action: null,
    id: null,
    data: null,
    status: null
});

const saveDraft = function (data, cb) {
    if (!data.toAddress || !data.fromAddress  || !data.user || !data.subject || !data.body || !data.campaign || !data.status) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    
    let email_data = {
        toUser: data.toAddress.join(','),
        from: data.fromAddress,
        cc: data.ccAddress,
        user: data.user,
        templateCategory: data.templateCategory,
        tags: data.tags,
        subject: data.subject,
        text: `${data.body.html} ${data.body.textMessage}`,
        campaign: data.campaign,
        status: data.status,
        open: data.open || 0,
        click: data.click || 0
    }
    emailhistory.insert(email_data, function (err, resp) {
        if (err) {
            console.log(err, err.stack);
            return cb(responseStruct.merge({
                success: false,
                message: `Something went wrong in saving draft to database`,
                status: 500,
                data: err
            }).toJS());
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `draft saved`,
            status: 200,
            data: resp
        }).toJS());
    })
}
exports.saveDraft = saveDraft


// update emailhistory
const updateEmailHistoryById = function (data, cb) {
    if (!data.status || !data.campaign || !data.user || !data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let updateData = {
        id: data.id,
        toUser: data.toAddress.join(','),
        from: data.fromAddress,
        cc: data.ccAddress,
        user: data.user,
        templateCategory: data.templateCategory,
        tags: data.tags,
        subject: data.subject,
        text: `${data.body.html} ${data.body.textMessage}`,
        campaign: data.campaign,
        status: data.status,
        open: data.open || 0,
        click: data.click || 0
    }
    emailhistory.update(updateData, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: ` emailhistory updated`,
            status: 200
        }).toJS());
    })
};

exports.updateEmailHistoryById = updateEmailHistoryById

// get emailhistory
const getEmailHistory = function (data, cb) {
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    emailhistory.getEmailsByUserId(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        if (resp.length > 0) {
            return cb(null, responseStruct.merge({
                success: true,
                data: resp,
                message: `EmailHistory found`,
                status: 200
            }).toJS());
        } else {
            return cb(null, responseStruct.merge({
                success: true,
                data: [],
                message: `no EmailHistory found`,
                status: 200
            }).toJS());
        }
    })
};

exports.getEmailHistory = getEmailHistory

// get emailhistory by id
const getEmailHistoryById = function (data, cb) {
    if (!data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    emailhistory.getEmailsById(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        if (resp.length > 0) {
            return cb(null, responseStruct.merge({
                success: true,
                data: resp,
                message: `email found`,
                status: 200
            }).toJS());
        } else {
            return cb(null, responseStruct.merge({
                success: true,
                data: [],
                message: `no email found`,
                status: 200
            }).toJS());
        }
    })
};

exports.getEmailHistoryById = getEmailHistoryById

// delete emailhistory by id
const deleteEmailHistoryById = function (data, cb) {
    if (!data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    emailhistory.delete(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `email history deleted`,
            status: 200
        }).toJS());

    })
};

exports.deleteEmailHistoryById = deleteEmailHistoryById
