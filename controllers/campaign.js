const mute = require('immutable');
const campaign = require('../models/campaign')


// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "campaign",
    action: null,
    id: null,
    data: null,
    status: null
});

// insert campaign
const insertCampaign = function (data, cb) {
    if (!data.name || !data.user || !data.endDate) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let insertData = {
        name: data.name,
        user: data.user,
        endDate: data.endDate
    }
    campaign.insert(insertData, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `new campaign added`,
            data: resp,
            status: 200
        }).toJS());
    })
};

exports.insertCampaign = insertCampaign

// update campaign
const updateCampaign = function (data, cb) {
    if (!data.user && !data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let updateData = {
        name: data.name,
        user: data.user,
        id: data.id,
        endDate: data.endDate
    }
    campaign.update(updateData, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: ` campaign updated`,
            status: 200
        }).toJS());
    })
};

exports.updateCampaign = updateCampaign

// get campaign
const getCampaign = function (data, cb) {
    if (!data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    campaign.get(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        // console.log(resp);

        if (resp.length > 0) {
            return cb(null, responseStruct.merge({
                success: true,
                message: `campaign found`,
                data: resp,
                status: 200
            }).toJS());
        } else {
            return cb(null, responseStruct.merge({
                success: true,
                message: `no campaigns found`,
                data: [],
                status: 200
            }).toJS());
        }
    })
};

exports.getCampaign = getCampaign

// get campaign by id
const getCampaignById = function (data, cb) {
    if (!data.id || !data.user) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    campaign.getById(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        if (resp.length > 0) {
            return cb(null, responseStruct.merge({
                success: true,
                message: `campaign found`,
                data: resp,
                status: 200
            }).toJS());
        } else {
            return cb(null, responseStruct.merge({
                success: true,
                message: `no campaigns found`,
                data: [],
                status: 200
            }).toJS());
        }
    })
};

exports.getCampaignById = getCampaignById

// delete campaign by id
const deleteCampaignById = function (data, cb) {
    if (!data.id) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    campaign.delete(data, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `campaign deleted`,
            status: 200
        }).toJS());

    })
};

exports.deleteCampaignById = deleteCampaignById
