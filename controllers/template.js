const mute = require('immutable');
const template = require('../models/template')


// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "template",
    action: null,
    id: null,
    data: null,
    status: null
});

// insert organiation
const insertTemplate = function (data, cb) {
    if (!data.name  ||!data.tags || !data.templateCategory || !data.body) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let insertData = {
        name: data.name,
        tags: data.tags,
        templateCategory: data.templateCategory,
        text: data.body
    }
    template.insert(insertData, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);    
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `new template added`,
            status: 200
        }).toJS());
    })
};

exports.insertTemplate = insertTemplate
