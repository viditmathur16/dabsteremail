const AWS = require('aws-sdk');
const mute = require('immutable');

const emailhistory = require('../models/emailhistory')

// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "sendEmail",
    action: null,
    id: null,
    data: null,
    status: null
});

AWS.config.secretAccessKey = process.env.AWS_SESCONFIG_SECRETACCESSKEY
AWS.config.accessKeyId = process.env.AWS_SESCONFIG_ACCESSKEY
AWS.config.region = process.env.AWS_SESCONFIG_REGION

const sesv2 = new AWS.SESV2({ apiVersion: process.env.AWS_SESCONFIG_APIVERSION });

const addNewEmail = function (data, cb) {
    if (!data.email) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }

    const params = {
        EmailIdentity: data.email
    };
    // if (data.tags) {
    //     params.Tags = data.tags

    // }
    sesv2.createEmailIdentity(params, function (err, data) {
        if (err) {
            console.log(err, err.stack);
            return cb(responseStruct.merge({
                success: true,
                message: `Something went wrong`,
                status: 500,
                data: err
            }).toJS());
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `email added`,
            status: 200,
            data: response
        }).toJS());
    });
}
exports.addNewEmail = addNewEmail


const sendEmail = function (data, cb) {
    console.log(data.toAddress, data.fromAddress, data.body, data.subject)
    if (!data.toAddress || !data.fromAddress || !data.body || !data.subject) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    if (!data.replyTo) {
        data.replyTo = [data.fromAddress]
    } else if (!Array.isArray(data.replyTo)) {
        data.replyTo = data.replyTo.split(',')
    }
    if (!Array.isArray(data.toAddress)) {
        data.toAddress = [data.toAddress]
    }
    if (data.ccAddress || !Array.isArray(data.ccAddress)) {
        data.ccAddress = [data.ccAddress]
    }
    let emailParams = {
        Content: {
            Simple: {
                Body: {
                    Html: {
                        Data: data.body.html || " "
                        // Charset: 'STRING_VALUE'
                    },
                    Text: {
                        Data: data.body.textMessage || " "
                    }
                },
                Subject: {
                    Data: data.subject || " ",
                    // Charset: 'STRING_VALUE'
                }
            }
        },
        Destination: {
            //   BccAddresses: [ ],
            CcAddresses: data.ccAddress || [],
            ToAddresses: data.toAddress
        },
        FromEmailAddress: data.fromAddress,
        ReplyToAddresses: data.replyTo,
        // Source: `vidit@zivost.com`,
        // ReturnPath: `vidit@zivost.com`
    }
    sesv2.sendEmail(emailParams, function (err, response) {
        if (err) {
            if (err.code === 'MessageRejected') {
                console.log(err, err.stack);
                return cb(responseStruct.merge({
                    success: false,
                    message: err.messages,
                    status: 400,
                    data: err
                }).toJS());
            } else {
                return cb(responseStruct.merge({
                    success: false,
                    message: err.messages,
                    status: 400,
                    data: err
                }).toJS());
            }
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `email sent`,
            status: 200,
            data: response
        }).toJS());

    })
}
exports.sendEmail = sendEmail


const getAccountInformation = function (data, cb) {

    const params = {
        EmailIdentity: data.email,
        // EndDate: data.endDate,
        // StartDate: data.startDate
    };
    sesv2.getEmailIdentity(params, function (err, response) {
        if (err) {
            console.log(err, err.stack);
            return cb(responseStruct.merge({
                success: true,
                message: `Something went wrong`,
                status: 500,
                data: err
            }).toJS());
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `account information found`,
            action: `get account information`,
            status: 200,
            data: response
        }).toJS());
    });
}
exports.getAccountInformation = getAccountInformation

