const mute = require('immutable');
const templateCategory = require('../models/templateCategory')


// Response Struct
const responseStruct = mute.Map({
    signature: null,
    success: null,
    message: "",
    type: "templateCategory",
    action: null,
    id: null,
    data: null,
    status: null
});

// insert organiation
const insertTemplateCategory = function (data, cb) {
    if (!data.name || !data.organisation ) {
        return cb(responseStruct.merge({
            success: false,
            message: `params missing`,
            status: 400,
        }).toJS());
    }
    let insertData = {
        name: data.name,
        organisation: data.organisation
    }
    templateCategory.insert(insertData, function (err, resp) {
        if (err) {
            console.log(err);
            throw (err);
        }
        return cb(null, responseStruct.merge({
            success: true,
            message: `new templateCategory added`,
            status: 200
        }).toJS());
    })
};

exports.insertTemplateCategory = insertTemplateCategory
