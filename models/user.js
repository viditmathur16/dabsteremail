const db = require('./db');
const moment = require('moment');
const table = 'users';


exports.insert = function (data, done) {
	let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
	let insertData = {
		firstName: data.firstName,
		lastName: data.lastName,
		username: data.username,
		accountId: data.accountId,
		designation: data.designation,
		salt: data.salt,
		email: data.email,
		mobile: data.mobile,
		organisation: data.organisation,
		gender: data.gender,
		birthday: data.birthday,
		isVerified: false,
		isDeleted: false,
		lastLogin: timestamp,
		created_at: timestamp,
		updated_at: timestamp
	};

	let queryBuilder = db.insert(insertData, 'id').into(table);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result);
	});
};


exports.getByEmail = function (data, done) {
	var queryBuilder = db.select().from(table);
	queryBuilder = queryBuilder.where('email', data.email);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
}

exports.patchProfile = function (data, done) {
	const timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
	let queryBuilder = db.where('id', data.user).update({
		bio: data.bio,
		designation: data.designation,
		updated_at: timestamp
	}).into(table);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
};

exports.updateLoginInfo = function (data, done) {
	var timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
	var queryBuilder = db.where('id', data.id).update({
		lastLogin: timestamp
	}).into(table);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
}

exports.readByKeyValue = function (data, done) {
	// TODO: limit stuff here
	let queryBuilder = db.select().from(table);

	if (data.isRaw) {
		queryBuilder = queryBuilder.whereRaw(`${data.key} = ${data.value}`)
	} else {
		queryBuilder = queryBuilder.where(data.key, data.value)
	}


	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
};

exports.delete_user = function (data, done) {
	const timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
	let queryBuilder = db.where('id', data.id).update({
		isDeleted: true,
		updated_at: timestamp
	}).into(table);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
}


exports.updateProfile = function (data, done) {
	const timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
	if (parseInt(data.isVisible) === 1) {
		data.isDeleted = 0
	}
	let queryBuilder = db.where('id', data.user).update({
		firstName: data.firstName,
		lastName: data.lastName,
		gender: data.gender,
		isDeleted: data.isDeleted,
		isDisabled: data.isDisabled,
		isVerified: data.isVerified,
		organisation: data.organisation,
		designation: data.designation,
		email: data.email,
		mobile: data.mobile,
		username: data.username,
		birthday: data.birthday,
		updated_at: timestamp
	}).into(table);
	queryBuilder.asCallback(function (err, result) {
		if (err) {
			return done(err);
		}
		done(null, result)
	});
};