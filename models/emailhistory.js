const db = require('./db');
const moment = require('moment');
const table = 'emailhistory';

exports.insert = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let insertData = {
        toUser: data.toUser,
        from: data.from,
        // cc: data.cc,
        user: data.user,
        templateCategory: data.templateCategory,
        tags: data.tags,
        subject: data.subject,
        text: data.text,
        campaign: data.campaign,
        status: data.status,
        open: data.open,
        click: data.click,
        created_at: timestamp,
        updated_at: timestamp
    };
    let queryBuilder = db.insert(insertData, 'id').into(table);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.getEmailsByUserId = function (data, done) {
    let selectArr =[
        `toUser`,
        `from`,
        `emailhistory.id`,
        `cc`,
        `user`,
        `templateCategory`,
        `tags`,
        `subject`,
        `text`,
        `campaign`,
        `c1.name as campaign_name`,
        `${table}.status`,
        `open`,
        `click`,
        `${table}.created_at`,
        `${table}.updated_at`
    ]
    let queryBuilder = db.select(selectArr).from(table);
    queryBuilder = queryBuilder.leftJoin(`campaign AS c1`,`${table}.campaign`,`c1.id`)
    queryBuilder = queryBuilder.where('emailhistory.user', data.user);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.getEmailsById = function (data, done) {
    let selectArr =[
        `toUser`,
        `emailhistory.id`,
        `from`,
        `cc`,
        `user`,
        `templateCategory`,
        `tags`,
        `subject`,
        `text`,
        `campaign`,
        `c1.name as campaign_name`,
        `${table}.status`,
        `open`,
        `click`,
        `${table}.created_at`,
        `${table}.updated_at`
    ]
    let queryBuilder = db.select(selectArr).from(table);
    queryBuilder = queryBuilder.leftJoin(`campaign AS c1`,`${table}.campaign`,`c1.id`)
    queryBuilder = queryBuilder.where('emailhistory.id', data.id);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.update = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let update_data = {
        toUser: data.toUser,
        from: data.from,
        // cc: data.cc,
        user: data.user,
        templateCategory: data.templateCategory,
        tags: data.tags,
        subject: data.subject,
        text: data.text,
        campaign: data.campaign,
        status: data.status,
        open: data.open,
        click: data.click,
        created_at: timestamp,
        updated_at: timestamp
    };
    let queryBuilder = db.where('id', data.id).update(update_data).into(table);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.delete = function (data, done) {
    let queryBuilder = db.from(table).where('id',data.id).del();
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};
