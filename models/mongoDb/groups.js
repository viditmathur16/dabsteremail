let mongoose = require('./db');

// grab the things we need
let Schema = mongoose.Schema;

// create a schema
let group = new Schema({
    id: String,
    user: String,
    name:String,
    status:String,
    isDeleted:Boolean,
    description:String,
    groupCode:String,
    created_at: Date,
    updated_at: Date
});
group.pre('save', function (next) {
    // get the current date
    let currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;
    // if created_at doesn't exist, add to that field
    if (!this.created_at) {
        this.created_at = currentDate;
    }

    // if last_accessed doesn't exist, add to that field
    if (!this.last_accessed) {
        this.last_accessed = currentDate;
    }
    next();
});


let env = process.env.DB_ENV || "development";
let groups = mongoose.model(`groups_${env}`, group);

module.exports = groups;
