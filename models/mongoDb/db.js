const mongoose = require('mongoose');
const host = process.env.DABSTER_MONGO_HOST;
const port = process.env.DABSTER_MONGO_PORT;
const user = process.env.DABSTER_MONGO_USER;
const pass = process.env.DABSTER_MONGO_PASS;


const opt = {
    user: user,
    pass: pass,
    authSource: "dabster-email-dev",
    useNewUrlParser: true,
    keepAlive: 1,
    reconnectTries: 30,
    connectTimeoutMS: 10000,
    socketTimeoutMS: (process.env.NODE_ENV === "production" ? 3000000 : 30000),
};

const connstring = `mongodb://${host}:${port}/dabster-email-dev`;

mongoose.connect(connstring, opt, function(err) {
    if (err) {
        console.error('Failed to connect to mongo on startup - retrying in 5 sec', err);
        setTimeout(connectWithRetry, 5000);
    }else{
        console.log("Connected to dabster_email MongoDB");
    }
});

module.exports = mongoose;