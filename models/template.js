const db = require('./db');
const moment = require('moment');
const table = 'template';

exports.insert = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let insertData = {
        name: data.name,
        organisation: data.organisation,
        tags: data.tags,
        templateCategory: data.templateCategory,
        text: data.text,
        created_at: timestamp,
        updated_at: timestamp
    };
    let queryBuilder = db.insert(insertData, 'id').into(table);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};
