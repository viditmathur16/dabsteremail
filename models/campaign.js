const db = require('./db');
const moment = require('moment');
const table = 'campaign';

exports.insert = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let insertData = {
        name: data.name,
        users: data.user,
        endDate: data.endDate,
        created_at: timestamp,
        updated_at: timestamp
    };
    let queryBuilder = db.insert(insertData, 'id').into(table);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.get = function (data, done) {
    let selectArr = [
        `${table}.id`,
        `${table}.name`,
        `${table}.name`,
        `e1.id as email_id`,
        `e1.toUser as email_recipents`,
        `${table}.status`,
        `e1.status as email_status`,
        `${table}.endDate as campaign_end_date`,
        `${table}.created_at as campaign_start_date`,
        `${table}.updated_at as campaign_update_date`,
        `e1.updated_at as email_send_date`,
        `e1.cc as email_cc`,
        `e1.from as email_from`,
        `e1.tags as email_tags`,
        `e1.subject as email_subject`,
        `e1.open as email_open`,
        `e1.click as email_click`,
        `e1.user as user`
    ]
    let queryBuilder = db.select(selectArr).from(table);
    queryBuilder = queryBuilder.leftJoin(`emailhistory AS e1`,`e1.campaign`,`${table}.id`)
    queryBuilder = queryBuilder.where('campaign.users',data.user)
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.getById = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let selectArr = [
        `${table}.name`,
        `${table}.id`,
        `e1.id as email_id`,
        `e1.toUser as email_recipents`,
        `${table}.status`,
        `e1.status as email_status`,
        `${table}.endDate as campaign_end_date`,
        `${table}.created_at as campaign_start_date`,
        `${table}.updated_at as campaign_update_date`,
        `e1.updated_at as email_send_date`,
        `e1.cc as email_cc`,
        `e1.from as email_from`,
        `e1.tags as email_tags`,
        `e1.subject as email_subject`,
        `e1.open as email_open`,
        `e1.click as email_click`,
        `e1.user as user`
    ]
    let queryBuilder = db.select(selectArr).from(table);
    queryBuilder = queryBuilder.leftJoin(`emailhistory AS e1`,`e1.campaign`,`${table}.id`)
    queryBuilder = queryBuilder.where('campaign.id',data.id)
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.update = function (data, done) {
    let timestamp = moment().format('YYYY-MM-DD HH:mm:ss');
    let update_data = {
        name: data.name,
        users: data.user,
        endDate: data.endDate,
        updated_at: timestamp
    };
    let queryBuilder = db.where('id', data.id).update(update_data).into(table);
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};

exports.delete = function (data, done) {
    let queryBuilder = db.from(table).where('id',data.id).del();
    queryBuilder.asCallback(function (err, result) {
        if (err) {
            return done(err);
        }
        done(null, result);
    });
};
