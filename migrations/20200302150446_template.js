
exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('template', function(table) {
            table.increments();
            table.string('name',30).unique();
            table.integer('users').unsigned().index().references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('templateCategory').unsigned().index().references('id').inTable('templateCategory').onDelete('CASCADE').onUpdate('CASCADE').nullable();
            table.string('tags');
            table.string('text');
            table.timestamps();
        })
    ])
};

exports.down = function(knex) {
    return Promise.all([
        knex.schema.dropTable('template')
    ])
};
