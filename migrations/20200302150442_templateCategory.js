
exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('templateCategory', function(table) {
            table.increments();
            table.string('name',30).unique();
            table.integer('users').unsigned().index().references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
            table.timestamps();
        })
    ])
};

exports.down = function(knex) {
    return Promise.all([
        knex.schema.dropTable('templateCategory')
    ])
};
