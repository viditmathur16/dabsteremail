
exports.up = function (knex) {
    return Promise.all([
        knex.schema.createTable('users', function (table) {
            table.increments();
            table.string('firstName',30);
            table.string('lastName',30).nullable();
            table.string('username',30).unique().nullable();
            table.string('accountId').unique();
            table.string('organisation').nullable();
            table.string('designation').nullable();
            table.string('mobile').nullable();
            table.string('email',150).unique();
            table.string('salt',500);
            table.string('gender',20);
            table.date('birthday').nullable();
            table.boolean('isVerified').defaultTo(0);
            table.boolean('isDisabled').defaultTo(0);
            table.boolean('isDeleted').defaultTo(0);
            table.dateTime('lastLogin').nullable();
            table.timestamps();
        })
    ])
};

exports.down = function (knex) {
    return Promise.all([
        knex.schema.dropTable('users')
    ])
};
