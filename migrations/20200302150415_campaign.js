
exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('campaign', function(table) {
            table.increments();
            table.string('name',30).unique();
            table.integer('users').unsigned().index().references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('status');
            table.date('endDate');
            table.timestamps();
        })
    ])
};

exports.down = function(knex) {
    return Promise.all([
        knex.schema.dropTable('campaign')
    ])
};
