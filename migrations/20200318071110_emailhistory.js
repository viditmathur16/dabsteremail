
exports.up = function(knex) {
    return Promise.all([
        knex.schema.createTable('emailhistory', function(table) {
            table.increments();
            table.string('toUser');
            table.string('from',30);
            table.string('cc').nullable();
            table.string('tags').nullable();
            table.string('subject');
            table.string('text').collate('utf8_unicode_ci');
            table.integer('user').unsigned().index().references('id').inTable('users').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('templateCategory').unsigned().index().references('id').inTable('templateCategory').onDelete('CASCADE').onUpdate('CASCADE');
            table.integer('campaign').unsigned().index().references('id').inTable('campaign').onDelete('CASCADE').onUpdate('CASCADE');
            table.string('status');
            table.integer('open').nullable();
            table.integer('click').nullable();
            table.timestamps();
        })
    ])
};

exports.down = function(knex) {
    return Promise.all([
        knex.schema.dropTable('emailhistory')
    ])
};
