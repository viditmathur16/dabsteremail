
exports.up = function(knex) {
  return Promise.all([
      knex.schema.alterTable('users', function(table){
        table.boolean('apiKeyAdded').defaultTo(0);
        table.string('apiVersion').nullable();
        table.string('accessKey').nullable();
        table.string('secretAccessKey').nullable();
        table.string('region').nullable();
      })
  ])
};

exports.down = function(knex) {
    return Promise.all([
        knex.schema.table('users', function(table){
          table.dropColumn('apiKeyAdded');
          table.dropColumn('apiVersion');
          table.dropColumn('accessKey');
          table.dropColumn('secretAccessKey');
          table.dropColumn('region');
        })
    ])
};
