let express = require('express');
let router = express.Router();
const templateCategory = require('../controllers/templateCategory')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);


router.post('/v1/templateCategory',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    templateCategory.insertTemplateCategory(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get all templateCategory
router.get('/v1/templateCategory',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    return res.status(status).send(response);
});

//get templateCategory by id
router.get('/v1/:id/templateCategory/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

//update templateCategory by id
router.patch('/v1/:id/templateCategory/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

//delete templateCategory by id
router.delete('/v1/:id/templateCategory/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

module.exports = router;