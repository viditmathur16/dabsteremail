let express = require('express');
let router = express.Router();
let addressBook = require('./../controllers/address')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

//post a new addressbook
router.post('/v1/addressbook',authenticator, function (req, res, next) {
    let data = {};
    data.addressBook = req.body;
    data.req = req.data;
    addressBook.insertAddressBook(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })

});

//fetch specific addresses in addressBook
router.get('/v1/addressbook',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    addressBook.fetchAddressBook(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//fetch groups in addressBook
router.get('/v1/addressbook/groups',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    addressBook.fetchAddressBookByGroups(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//fetch specific entry details
router.get('/v1/:id/addressbook/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    addressBook.fetchAddressBookById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//fetch distinct groups
router.get('/v1/addressbook/uniquegroups',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    addressBook.getDistinctAddressGroups(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//update specific entry details
router.patch('/v1/:id/addressbook/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    addressBook.updateAddressBook(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//update specific entry group details
router.patch('/v1/:id/addressbook/groups',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    addressBook.addAddressBookGroups(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//update group in multiple entries
router.patch('/v1/groups',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    addressBook.addGroupToMultipleAddress(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//remove group in multiple entries
router.patch('/v1/groups/delete',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    addressBook.deleteGroupToMultipleAddress(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//delete specific entry details
router.delete('/v1/:id/addressbook/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    addressBook.deleteAddress(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

module.exports = router;