var express = require('express');
var router = express.Router();


const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);


let healthResponse = {
    title: "Dabster Email Service",
    service: "email",
    health: "ok",
    env: process.env.DB_ENV
};

/* GET home page. */
router.get('/', function(req, res, next) {
    res.status(200).send(healthResponse);
});
router.get('/chat/v1/health', function(req, res, next) {
    res.status(200).send(healthResponse);
});

module.exports = router;