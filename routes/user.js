let express = require('express');
let router = express.Router();
const users = require('../controllers/user')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);


// router.post('/v1/user', function (req, res, next) {
//     let data = req.body;
//     data.req = req.data;
//     user.insertUser(data, function (err, response) {
//         let status = 0;
//         if (err) {
//             console.log(err);
//             status = err.status;
//             return res.status(status).send(err);
//         }
//         status = response.status;
//         return res.status(status).send(response);
//     })
// });

// /* POST user logins. */
router.post('/v1/users/login', function(req, res, next) {
    let data = req.body;
    data.req = req.data;
    
    users.userLogin(data, function(err, response) {
      var status = 0;
      if (err) {
        status = err.status;
        return res.status(status).send(err);
      }
      status = response.status;
      return res.status(status).send(response);
    });
  });

// /* POST user signups. */
router.post('/v1/users/signup', function(req, res, next) {
    let data = req.body;
    data.req = req.data;

    users.userSignup(data, function(err, response) {
      var status = 0;
      if (err) {
        status = err.status;
        return res.status(status).send(err);
      }
      status = response.status;
      return res.status(status).send(response);
    });
  });

  // /* update user. */
router.patch('/v1/:id/users/', function(req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;

    users.updateProfile(data, function(err, response) {
      var status = 0;
      if (err) {
        status = err.status;
        return res.status(status).send(err);
      }
      status = response.status;
      return res.status(status).send(response);
    });
  });

module.exports = router;