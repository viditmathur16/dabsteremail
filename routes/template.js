let express = require('express');
let router = express.Router();
const template = require('../controllers/template')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);


//add new templates
router.post('/v1/template',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    template.insertTemplate(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get all templates
router.get('/v1/template',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    return res.status(status).send(response);
});

//get all templates by id
router.get('/v1/:id/template/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

//update a templates by id
router.patch('/v1/:id/template/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

//update a templates by id
router.patch('/v1/:id/template/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

//delete a templates by id
router.delete('/v1/:id/template/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    return res.status(status).send(response);
});

module.exports = router;