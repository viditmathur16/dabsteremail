let express = require('express');
let router = express.Router();
const sendEmail = require('../controllers/sendEmail')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

//send email
router.post('/v1/sendEmail',authenticator, function (req, res, next) {
    let data = req.body;
     
    data.req = req.data;
    sendEmail.sendEmail(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get account info
router.get('/v1/accountinfo',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    sendEmail.getAccountInformation(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

module.exports = router;