let express = require('express');
let router = express.Router();
const campaign = require('../controllers/campaign')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

//post new campaign
router.post('/v1/campaign',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    campaign.insertCampaign(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get all campaigns
router.get('/v1/campaign',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    campaign.getCampaign(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get campaign by id
router.get('/v1/:id/campaign/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;

    campaign.getCampaignById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//update campaign by id
router.patch('/v1/:id/campaign/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    campaign.updateCampaign(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//delete campaign by id
router.delete('/v1/:id/campaign/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    campaign.deleteCampaignById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

module.exports = router;