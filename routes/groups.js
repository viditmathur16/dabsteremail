let express = require('express');
let router = express.Router();
let groups = require('./../controllers/group')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

//post a new group
router.post('/v1/group',authenticator, function (req, res, next) {
    let data = {};
    data = req.body;
    data.req = req.data;
    groups.insertGroup(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })

});

//fetch groups
router.get('/v1/groups',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    groups.fetchGroups(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//fetch specific group details
router.get('/v1/:id/group/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    groups.fetchGroupById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});


//update specific entry details
router.patch('/v1/:id/group/',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    groups.updateGroup(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});


//delete specific group details
router.delete('/v1/:id/group/',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    groups.deleteGroup(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

module.exports = router;