let express = require('express');
let router = express.Router();
const emailhistory = require('../controllers/emailhistory')

const formatRequest = require('../middlewares/formatRequest');
router.use(formatRequest);
const clients = {
    users: {
        host: process.env.SERVICE_RPC_HOST,
        port: process.env.PK_USER_PORT
    }
};
const data = {};
const authenticator = require('../middlewares/authenticator')(clients, data);

//adding new email draft
router.post('/v1/email',authenticator, function (req, res, next) {
    let data = req.body;
    data.req = req.data;
    emailhistory.saveDraft(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get email by user
router.get('/v1/email',authenticator, function (req, res, next) {
    let data = req.query;
    data.req = req.data;
    emailhistory.getEmailHistory(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//get email by id
router.get('/v1/:id/email',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    emailhistory.getEmailHistoryById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//update email by id
router.patch('/v1/:id/email',authenticator, function (req, res, next) {
    let data = req.body;
    data.id = req.params.id;
    data.req = req.data;
    emailhistory.updateEmailHistoryById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

//delete email by id
router.delete('/v1/:id/email',authenticator, function (req, res, next) {
    let data = req.query;
    data.id = req.params.id;
    data.req = req.data;
    emailhistory.deleteEmailHistoryById(data, function (err, response) {
        let status = 0;
        if (err) {
            console.log(err);
            status = err.status;
            return res.status(status).send(err);
        }
        status = response.status;
        return res.status(status).send(response);
    })
});

// //get account info
// router.get('/v1/accountinfo',authenticator, function (req, res, next) {
//     let data = req.query;
//     data.req = req.data;
//     sendEmail.getAccountInformation(data, function (err, response) {
//         let status = 0;
//         if (err) {
//             console.log(err);
//             status = err.status;
//             return res.status(status).send(err);
//         }
//         status = response.status;
//         return res.status(status).send(response);
//     })
// });

module.exports = router;